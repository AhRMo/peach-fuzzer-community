## Peach Fuzzer Community Edition v3

### Newer version

A newer version of this fuzzer is available [as the GitLab Protocol Fuzzer Community Edition here](https://gitlab.com/gitlab-org/security-products/protocol-fuzzer-ce).

### Historical Version

Welcome to Peach 3, a complete re-write of Peach using the Microsoft.NET framework.

Peach Community 3 is a cross-platform fuzzer capable of performing both dumb and smart fuzzing. Peach includes a robust monitoring system allowing for fault detection, data collection, and automation of the fuzzing environment.

Peach does not target one specific class of target, making it adaptable to fuzz any form of data consumer. Peach is commonly used to fuzz file formats, network protocols, and APIs. With targets ranging from web browsers and network services through mobile devices, industrial control systems (SCADA) and even down at the silicon level.

All features of Peach are designed to be easily extended. This include mutation algorithms, data types, I/O adapters, monitoring modules, etc.

Extensions to Peach are typically written in the C# language as assembly modules that are identified through reflection.

Peach has been in active development since 2004. The first version of Peach was written while drinking beer at PH-Neutral in Berlin.

Features

 * Data definition written in XML
 * Smart & Dumb fuzzing
 * Analyzers to help automate fuzzer creation
 * Support for attaching debuggers, network capture, vm control
 * GUI Validation Tool

Peach 3 currently supports the following operating systems:

 * Windows
 * OS X
 * Linux (e.g. Ubuntu, Redhat, etc.)

### Documentation

The [Peach Fuzzer Community documentation is available
here](http://community.peachfuzzer.com).

### Installing binaries distribution

The [latest binaries can be found here](https://sourceforge.net/projects/peachfuzz/files/Peach/).

Extract archive and your ready to go!

### Installing from Source

Windows Pre-requisites:
  * Microsoft.NET v4
  * Visual Studio 2010 SP1
  * Python 2.7

Linux Pre-requisites:  
  * build-essential
  * mono-complete
  * g++-multilib (x86_64 only)
  * Python 2.7

OS X Pre-requisites:
  * XCode 4
  * Mono SDK (2.10.10)
  * Python 2.7

```sh
./waf configure
./waf build
./waf install
```
